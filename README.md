# Drinkts

Cocktail-related types.

Based off of [https://github.com/joseluisq/hyperapp-starter.git](hyperapp-starter)

## Features

- [x] [Typescript](https://www.typescriptlang.org/) with TSX
- [x] [Parcel](https://github.com/parcel-bundler/parcel) bundler
- [x] [CSS/Sass/SCSS](https://github.com/sass/node-sass) support
- [x] [PostCSS with Autoprefixer](https://github.com/postcss/autoprefixer)
- [x] [Jest](http://jestjs.io/) testing with [Typescript support](https://github.com/kulshekhar/ts-jest)
- [x] [TSLint](https://github.com/palantir/tslint) with [Standard Plus](https://github.com/joseluisq/tslint-config-standard-plus) rules
- [x] [VS Code User Workspace](https://code.visualstudio.com/docs/getstarted/settings) pre-configured and ready to use


### Development

```
yarn start
```

### Production

```
yarn build
```

### Testing

```
yarn test
```

## License
MIT license
