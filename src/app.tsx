import { ActionsType, h, View } from "hyperapp"
import { Actions, State } from "@app/types"
import { Store } from "@app/store"
import { CounterComponent as Counter } from "@components/counter"
import { PossibleDrinksComponent } from "@components/PossibleDrinks"
import { HasAndCanAquireComponent } from "@components/HasAndCanAquire"
import { Logo } from "@components/logo"
import "@styles/style.scss"

export const state: State = Store.state
export const actions: ActionsType<State, Actions> = Store.actions

export const view: View<State, Actions> = (state, actions) => (
  <main>
    <div class="container">
      <div class="leftPane">
        <HasAndCanAquireComponent state={state} actions={actions} />
      </div>
      <div class="middlePane">
        <PossibleDrinksComponent state={state} actions={actions} />
      </div>
      <div class="rightPane">
        <div>{"Favorite drinks:"}</div>
        {state.favoriteDrinks.map((favoriteDrink: string) => (
          <div>{favoriteDrink}</div>
        ))}
      </div>
    </div>
    {/* <p><Logo /></p> */}
    <Counter label={state.count} {...actions} />
  </main>
)
