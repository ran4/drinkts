import { ActionsType } from "hyperapp"
import { Actions, Ingredient, IngredientType, Recipe, RecipeIngredient, State } from "@app/types"
import ingredients from "./data/ingredients"
import recipes from "./data/recipes"

export namespace Store {
	export const state: State = {
  count: 0,
  has: [
    ingredients.plymouthSloeGin,
    ingredients.gin,
    ingredients.tonicWater,
  ],
  canAquire: [
    ingredients.simpleSyrup,
  ],
  recommended: [
    ingredients.cointreu,
    ingredients.limeJuice,
    ingredients.lemonJuice,
    ingredients.tequila,
    ingredients.agaveNectar,
    ingredients.rosesLime,
  ],
  possibleDrinks: [
    recipes.ginTonic,
    recipes.sweetOrange,
  ],
  favoriteDrinks: [
    recipes.sweetOrange.name,
  ],
}

	const calculatePossibleDrinks = (has: Ingredient[]) => (recommended: Ingredient[]) => {
  const hasIngredientTypes: string[] = has.map((ingredient: Ingredient) => ingredient.type)

  return Object.values(recipes).filter((recipe: Recipe, idx: number) => {
    const nonOptionalRecipeIngredients: RecipeIngredient[] =
				recipe.ingredients
				.filter((ri: RecipeIngredient) =>
					typeof ri.optional === "undefined" || ri.optional !== true)
    console.log("nonOptionalRecipeIngredients: " + JSON.stringify(nonOptionalRecipeIngredients))

    return nonOptionalRecipeIngredients.every((recipeIngredient: RecipeIngredient) =>
				hasIngredientTypes.includes(recipeIngredient.ingredient.type))
  })
		// [ recipes.ginTonic ]
}

	export const actions: ActionsType<State, Actions> = {
  down: () => (state) => ({ count: state.count - 1 }),
  up: () => (state) => ({ count: state.count + 1 }),
  removeHas: (idx: number) => (state) => {
    const newHas = state.has.filter((_ingredient, i) => idx !== i)
    const newRecommended = [ ...state.recommended, state.has[idx] ]
    return {
      has: newHas,
      recommended: newRecommended,
      possibleDrinks: calculatePossibleDrinks(newHas)(newRecommended),
    }
  },
  removeCanAquire: (idx: number) => (state) => {
    const newCanAquire = state.canAquire.filter((_ingredient, i) => idx !== i)
    const newRecommended = [ ...state.recommended, state.canAquire[idx] ]
    return {
      canAquire: newCanAquire,
      recommended: newRecommended,
      possibleDrinks: calculatePossibleDrinks(state.has)(newRecommended),
    }
  },
  addRecommendedIngredient: (idx: number) => (state) => {
    const newHas = [ ...state.has, state.recommended[idx] ]
    const newRecommended = state.recommended.filter((_, i) => idx !== i)
			// https://vincent.billey.me/pure-javascript-immutable-array/
    return {
      has: newHas,
      recommended: newRecommended,
      possibleDrinks: calculatePossibleDrinks(newHas)(newRecommended),
    }
  },
  addFavoriteDrink: (drinkName: string) => (state) => ({
    favoriteDrinks: state.favoriteDrinks.includes(drinkName)
				? state.favoriteDrinks
				: [ ...state.favoriteDrinks, drinkName ],
  }),
  removeFavoriteDrink: (drinkName: string) => (state) => ({
    favoriteDrinks: state.favoriteDrinks.filter((drinkNameToRemove) => drinkNameToRemove !== drinkName),
  }),
}
}
