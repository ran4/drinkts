import { Component, h } from "hyperapp"

import "./style.scss"

export const SectionComponent = ({
  id, label, topBarButtons, body,
}: {
  id: string,
  label: string,
  topBarButtons: JSX.Element[],
  body: JSX.Element,
}) => (
  <div class="section" id={id} >
    <div class="section__topBar">
      <span class="section__topBar__label">{label}</span>
      {topBarButtons}
    </div>
    {body}
  </div >
)
