import { Component, h } from "hyperapp"
import { Actions, Button, HasAndCanAquirePane, Ingredient, State } from "@app/types"
import { SectionComponent } from "@components/Section"
import "./style.scss"

export const AddButton: Component<Button> = ({ label, action, disabled }) => (
  <button class="addButton" onclick={() => action()} disabled={disabled}>{label}</button>
)

export const RecommendedIngredientsListing = ({
  ingredients, action,
}: {
  ingredients: Ingredient[],
  action: (idx: number) => State,
}) => (
  <div class="ingredientListing">
    {ingredients.map((ingredient: Ingredient, idx: number) => (
      <p class="ingredientListing__line">
        {ingredient.type}
        <span class="ingredientListing__line__add"
              onclick={() => action(idx)}
              >
          {"Add"}
        </span>
      </p>))
    }
  </div>
)

export const IngredientListing = ({
  ingredients, action,
}: {
  ingredients: Ingredient[],
  action: (idx: number) => State,
}) => (
  <div class="ingredientListing">
    {ingredients.map((ingredient: Ingredient, idx: number) => (
      <p class="ingredientListing__line">
        {ingredient.type}
        <span class="ingredientListing__line__remove"
              onclick={() => action(idx)}
              >
          {"Remove"}
        </span>
      </p>))
    }
  </div>
)

export const HasAndCanAquireComponent: Component<HasAndCanAquirePane> = ({
    state, actions,
  }) => (
  <div class="item">
    <SectionComponent
      id="hasSection"
      label="Has"
      topBarButtons={[
        <AddButton label="New" action={actions.up} disabled={false} />,
      ]}
      body={(<IngredientListing ingredients={state.has} action={actions.removeHas}/>)}
    />
    <SectionComponent
      id="canAquireSection"
      label="Can Aquire"
      topBarButtons={[]}
      body={(<IngredientListing ingredients={state.canAquire} action={actions.removeCanAquire} />)}
    />
    <SectionComponent
      id="recommendedSection"
      label="Recommended ingredients"
      topBarButtons={[]}
      body={(<RecommendedIngredientsListing ingredients={state.recommended}
                                            action={actions.addRecommendedIngredient} />)}
    />
  </div >
)
