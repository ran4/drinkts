import { Component, h } from "hyperapp"
import { Actions, PossibleDrinks, Recipe, RecipeIngredient } from "@app/types"
import { SectionComponent } from "@components/Section"
import "./style.scss"

export const RecipeListing = ({
  recipes, favoriteDrinks, addFavoriteDrink, removeFavoriteDrink,
}: {
  recipes: Recipe[],
  favoriteDrinks: string[],
  addFavoriteDrink: Actions["addFavoriteDrink"],
  removeFavoriteDrink: Actions["removeFavoriteDrink"],
}) => {
  console.log(`favoriteDrinks: ${favoriteDrinks.join(", ")}`)
  return recipes.map((recipe: Recipe) => {
    const instructions: string[] = Array.isArray(recipe.instruction)
    ? recipe.instruction
    : ([ recipe.instruction ])
    const isFavoriteDrink: boolean = favoriteDrinks.includes(recipe.name)
    return (
    <div class="recipeListing">
      <p class="recipeListing__label">
        <span class="recipeListing__label__text">{recipe.name}</span>
        <span class={`recipeListing__label__star ${isFavoriteDrink ? "isFavoriteDrink" : "isNotFavoriteDrink"}`}
              onclick={() => isFavoriteDrink ? removeFavoriteDrink(recipe.name) : addFavoriteDrink(recipe.name)
              }>☆</span>
      </p>
      <p class="recipeListing__ingredients">
        {recipe.ingredients.map((ri: RecipeIngredient) => {
          return (
            <p class="recipeListing__ingredients__line">
              <span class="garnish">{ri.isGarnish && "Garnish: "}</span>
              <span class="amount">{ri.amount ? ri.amount + " cl " : ""}</span>
              {ri.ingredient.type}
            </p>
          )
        })}
      </p>
      <p class="recipeListing__instructions">
        {instructions.map((instructionLine: string) => (
          <p class="recipeListing__instructions__line">{instructionLine}</p>
        ))}
      </p>
    </div >
    )
  })
}

export const PossibleDrinksComponent: Component<PossibleDrinks> = ({
    state, actions,
  }) => (
  <div class="item">
    <SectionComponent
      id="possibleDrinksSection"
      label="Possible drinks"
      topBarButtons={[]}
      body={(<RecipeListing recipes={state.possibleDrinks}
                            favoriteDrinks={state.favoriteDrinks}
                            addFavoriteDrink={actions.addFavoriteDrink}
                            removeFavoriteDrink={actions.removeFavoriteDrink}/>)}
    />
  </div >
)
