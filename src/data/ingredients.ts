import { AlcoholFreeIngredient, Gin, Ingredient, IngredientType } from "@app/types"

export const gin: Ingredient = {
  type: IngredientType.Gin,
  alcoholContent: 40,
}

export const tanquerayLondonDryGin: Gin = {
  type: IngredientType.Gin,
  alcoholContent: 43.1,
}

export const plymouthSloeGin: Ingredient = {
  type: IngredientType.SloeGin,
  brand: "Plymouth",
  alcoholContent: 26,
}

export const caolIla12Years: Ingredient = {
  type: "Islay whiskey",
  brand: "Caol Ila 12 Years",
  alcoholContent: 43,
}

export const tripleSec: Ingredient = {
  type: IngredientType.TripleSec,
  alcoholContent: 40,
}

export const cointreu: Ingredient = {
  type: IngredientType.TripleSec,
  brand: "cointreu",
  alcoholContent: 40,
}

export const rosesLime: AlcoholFreeIngredient = {
  type: IngredientType.LimeCordial,
  brand: "Rose's Lime",
  alcoholContent: null,
}

export const tequila: Ingredient = {
  type: IngredientType.Tequila,
  alcoholContent: 40,
}

export const tranberryJuice: AlcoholFreeIngredient = {
  type: IngredientType.TranberryJuice,
  alcoholContent: null,
}

export const tonicWater: AlcoholFreeIngredient = {
  type: IngredientType.TonicWater,
  alcoholContent: null,
}

export const orangeJuice: AlcoholFreeIngredient = {
  type: IngredientType.OrangeJuice,
  alcoholContent: null,
}

export const lemonJuice: AlcoholFreeIngredient = {
  type: IngredientType.Lemon,
  alcoholContent: null,
}

export const limeJuice: AlcoholFreeIngredient = {
  type: IngredientType.Lime,
  alcoholContent: null,
}

export const limeWedge: AlcoholFreeIngredient = {
  type: IngredientType.Lime,
  alcoholContent: null,
}

export const limeWheel: AlcoholFreeIngredient = {
  type: IngredientType.Lime,
  alcoholContent: null,
}

export const limeCordial: AlcoholFreeIngredient = {
  type: IngredientType.LimeCordial,
  alcoholContent: null,
}

export const orangeSlice: AlcoholFreeIngredient = {
  type: IngredientType.OrangeSlice,
  alcoholContent: null,
}

export const simpleSyrup: AlcoholFreeIngredient = {
  type: IngredientType.SimpleSyrup,
  alcoholContent: null,
}

export const agaveNectar: AlcoholFreeIngredient = {
  type: IngredientType.AgaveNectar,
  alcoholContent: null,
}

export default {
  gin,
  plymouthSloeGin,
  caolIla12Years,
  tripleSec,
  cointreu,
  tequila,
  rosesLime,
  tonicWater,
  tranberryJuice,
  orangeJuice,
  lemonJuice,
  limeJuice,
  limeCordial,
  limeWedge,
  limeWheel,
  orangeSlice,
  simpleSyrup,
  agaveNectar,
}
