import { AlcoholFreeRecipe, Equipment, GlassType, Ingredient, IngredientType, Recipe } from "@app/types"
import ingredients from "./ingredients"

const ginTonic: Recipe = {
  name: "Gin and tonic",
  ingredients: [
    {
      ingredient: ingredients.gin,
      amount: 4,
    },
    {
      ingredient: ingredients.tonicWater,
      amount: 16,
    },
    {
      ingredient: ingredients.limeWedge,
      optional: true,
    },
  ],
  instruction: [
    "Pour gin into an highball glass",
    "Add tonic water",
    "Garnish with a lime wedge",
  ],
  glassType: GlassType.Highball,
}

const sloeGinTonic: Recipe = {
  name: "Sloe Gin and tonic",
  ingredients: [
    {
      ingredient: ingredients.plymouthSloeGin,
      amount: 4,
    },
    {
      ingredient: ingredients.simpleSyrup,
      amount: 16,
    },
    {
      ingredient: ingredients.limeWedge,
      optional: true,
    },
  ],
  instruction: [
    "Pour gin into an highball glass",
    "Add tonic water",
    "Garnish with a lime wedge",
  ],
  glassType: GlassType.Highball,
}

const tranberryLemonBash: AlcoholFreeRecipe = {
  name: "Transberry Lemon bash",
  ingredients: [
    {
      ingredient: ingredients.tranberryJuice,
      amount: 30,
    },
    {
      ingredient: ingredients.lemonJuice,
      amount: 4,
    },
  ],
  instruction: [
    "Put tranberry juice and lemon juice in a glass",
  ],
  glassType: GlassType.Highball,
}

const sweetOrange: Recipe = {
  name: "Sweet Orange",
  ingredients: [
    {
      ingredient: ingredients.tripleSec,
      amount: 3,
    },
    {
      ingredient: ingredients.tequila,
      amount: 3,
    },
    {
      ingredient: ingredients.orangeJuice,
      amount: 6,
    },
    {
      ingredient: ingredients.tonicWater,
    },
    {
      ingredient: ingredients.orangeSlice,
      optional: true,
      isGarnish: true,
    },
  ],
  instruction: [
    "Into a cocktail shaker add triple sec, tequila, fresh squeezed orange juice, tonic water, and several ice cubes",
    "Stir the cocktail with a long-handled bar spoon vigorously for about 30 seconds.",
    // tslint:disable-next-line:max-line-length
    "Strain into a chilled margarita glass filled with ice (be sure you crush the ice up first. We used our food processor.)",
    "Garnish with a slice of navel orange.",
  ],
  glassType: GlassType.Margarita,
  images: [
    {
      url: "https://files.ellematovin.se/uploads/tequila-drink.jpg",
    },
  ],
  equipment: [ Equipment.CocktailShaker ],
}

const margarita: Recipe = {
  name: "Margarita",
  ingredients: [
    { ingredient: ingredients.tequila, amount: 6 },
    { ingredient: ingredients.lemonJuice, amount: 3 },
    { ingredient: ingredients.agaveNectar, amount: 1.5 },
    { ingredient: ingredients.limeWheel, optional: true, isGarnish: true },
  ],
  instruction: [
    "Add all the ingredients into a shaker with ice and shake vigorously",
    "Double-strain into a rocks glass over ice",
    "Garnish with a lime wheel",
  ],
  glassType: GlassType.Rocks,
  images: [
    {
      url: "https://cdn.liquor.com/wp-content/uploads/2018/09/14114835/PowersBrands_Elevated-Margarita_720x720-300x300.jpg",
    },
  ],
  equipment: [ Equipment.CocktailShaker ],
}

const gimlet: Recipe = {
  name: "Gimlet",
  ingredients: [
    { ingredient: ingredients.plymouthSloeGin, amount: 1.5 },
    { ingredient: ingredients.limeJuice, amount: 1.5 },
    { ingredient: ingredients.limeCordial, amount: 1.5 },
    { ingredient: ingredients.limeWheel, optional: true, isGarnish: true },
  ],
  instruction: [
    "Shake with ice and strain into a chilled coupe",
    "Garnish with a lime wheel",
  ],
  glassType: GlassType.Coupe,
  images: [ { url: "https://i.imgur.com/lt2hKBH.jpg" } ],
  equipment: [ Equipment.CocktailShaker ],
  source: "The PDT Cocktail Book, Jim Meehan, 2011",
}

const recipes: {[recipeName: string]: Recipe} = {
  ginTonic,
  sloeGinTonic,
  tranberryLemonBash,
  sweetOrange,
  margarita,
  gimlet,
}

export const alcoholFreeRecipes: {[recipeName: string]: AlcoholFreeRecipe} = {
  tranberryLemonBash,
}

export default recipes
