export enum IngredientType {
  Gin = "Gin",
  SloeGin = "Sloe Gin",
  Bourbon = "Bourbon",
  Tequila = "Tequila",
  TripleSec = "Triple Sec",
  Vodka = "Vodka",
  DarkRum = "Dark Rum",
  LightRum = "Light Rum",
  OrangeSlice = "Orange slice",
  OrangeJuice = "Orange Juice",
  TonicWater = "Tonic Water",
  SimpleSyrup = "Simple Syrup",
  TranberryJuice = "Tranberry Juice",
  Lemon = "Lemon",
  Lime = "Lime",
  LimeCordial = "Lime Cordial", // e.g. Rose's Lime
  AgaveNectar = "Agave nectar",
}

export interface Ingredient {
  type: IngredientType | string
  brand?: string
  alcoholContent?:
		| number // Same as the number in { abv: number }, e.g. Alcohol by volume
		| { abv: number} // Alcohol by volume
    | { abw: number } // Alcohol by weight
    | null // Use this instead of 0
}

export interface Gin extends Ingredient {
  type: IngredientType.Gin
}

export interface CitrusFruit extends Ingredient {
  type: IngredientType.Lemon | IngredientType.Lime
}

export interface AlcoholFreeIngredient extends Ingredient {
  alcoholContent: null
}

export enum GlassType {
  Tumbler,
  Rocks,
  Highball,
  Martini,
  Margarita,
  Coupe,
}

export interface RecipeIngredient {
  ingredient: Ingredient,
  amount?:
    | number // Equivalent to { cl: number }
    | { cl: number }
    | { ml: number }
    | { oz: number }
    | { count: number }
  optional?: true,
  isGarnish?: true,
}

export interface AlcoholFreeRecipeIngredient extends RecipeIngredient {
  ingredient: AlcoholFreeIngredient,
}

export enum Equipment {
  CocktailShaker,
}

type RecipeImage = {
  url?: string,
}

export interface Recipe {
  name: string,
  ingredients: RecipeIngredient[],
  instruction:
    | string
    | string[]
  glassType?: GlassType
  images?: RecipeImage[],
  equipment?: Equipment[],
  source?: string,  // e.g. which book the recipe is from
}

export interface AlcoholFreeRecipe extends Recipe {
  ingredients: AlcoholFreeRecipeIngredient[],
}

export interface TumblerRecipe extends Recipe {
  glassType: GlassType.Tumbler
}

////////////////////////////////////////////////////////////////////////////////

export interface State {
  count: number
  has: Ingredient[]
  canAquire: Ingredient[]
  recommended: Ingredient[]
  possibleDrinks: Recipe[]
  favoriteDrinks: string[]
}

export interface Actions {
  removeHas: (idx: number) => State
  removeCanAquire: (idx: number) => State
  addRecommendedIngredient: (idx: number) => State
  addFavoriteDrink: (drinkName: string) => State
  removeFavoriteDrink: (drinkName: string) => State
  down (): State
  up (): State
}

export interface Label {
  label: number | string
}

export interface Button extends Label {
  disabled?: boolean
  action (): State
}

export interface Counter extends Actions, Label {}

export interface HasAndCanAquirePane {
  state: State
  actions: Actions
}

export interface PossibleDrinks {
  state: State
  actions: Actions
}
